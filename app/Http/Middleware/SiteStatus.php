<?php
namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;

class SiteStatus
{
    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        if (null !== $user && ($user->hasrole('root') || $user->hasrole('admin')||user()->isImpersonated())) {
            return $next($request);
        }

        if(Setting::getValue('site-on') != 'on' && !\Route::is('login') ){
            return response()->view('customer.disabled');
        }

        return $next($request);
    }
}
