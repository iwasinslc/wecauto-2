<?php
return [
    'order_created'       => 'La orden #:id :amount :currency fue creada con éxito',
    'order_closed'        => 'La orden #:id :amount :currency fue cerrada a causa de ausencia de los fondos en el saldo',
    'sale'                => 'La venta :amount :currency se realizó con éxito',
    'purchase'            => 'Compra :amount :currency se realizó con éxito',
    'partner_accrue'      => 'Usted acaba de recibir la comisión de socio :amount :currency del usuario :login, en el nivel :level',
    'wallet_refiled'      => 'Su portamonedas fue recargado :amount :currency',
    'rejected_withdrawal' => 'Su retiro a un monto de :amount :currency fue cancelado.',
    'approved_withdrawal' => 'Su retiro a un monto de :amount :currency fue confirmado.',
    'new_partner'         => 'Usted tiene a un nuevo socio :login en el nivel :level',
    'parking_bonus'       => 'Bono por parking :amount :currency',
    'licence_cash_back'   => 'Cashback por la compra de una licencia :amount :currency'
];