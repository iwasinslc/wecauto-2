<?php
$types = \App\Models\TransactionType::get();

?>

<section class="lk-table table-filter-hidden">
    <div class="container">
        <h3 class="lk-title">{{__('Operations list')}}
        </h3>
        <form class="filter-form">
{{--            <label class="select">--}}
{{--                <select id="type">--}}
{{--                    <option value="">{{__('All')}}</option>--}}
{{--                    @foreach($types as $type)--}}
{{--                        <option value="{{$type->name}}">{{__($type->name)}}</option>--}}
{{--                    @endforeach--}}

{{--                </select>--}}
{{--            </label>--}}
            <label class="field-datepicker">
                <input id="date_from" class="js-datepicker" type="text" placeholder="{{__('From date')}}">
            </label>
            <label class="field-datepicker">
                <input id="date_to" class="js-datepicker" type="text" placeholder="{{__('Till date')}}">
            </label>
            <button class="btn"><svg class="svg-icon">
                    <use href="/assets/icons/sprite.svg#icon-search"> </use>
                </svg>
            </button>
        </form>

        <table class="responsive nowrap" id="operations-table">
            <thead>
            <tr>
                <th>{{__('Date')}}</th>
                <th>{{__('Type')}}</th>
                <th>{{__('Currency')}}</th>
                <th>{{__('Amount')}}</th>
                <th>{{__('Status')}}</th>
            </tr>
            </thead>
        </table>
    </div>
</section>



@push('load-scripts')
    <script>
        //initialize basic datatable
        table = $('#operations-table').width('100%').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[0, "desc"]],
            "ajax": {
                "url": "{{route('profile.operations.dataTable')}}",
                // "contentType": "application/json",
                "method": "GET",
                "data"   : function( d ) {
                    d.type= $('#type').val();
                    d.date_from= $('#date_from').val();
                    d.date_to= $('#date_to').val();
                },
                'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            },

            "columns": [
                {"data": "created_at"},
                {"data": "type_name"},
                {"data": "currency.name"},
                {
                    "data": 'amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return row['amount'] + row['currency']['symbol'];
                    }
                },


                {
                    "data": "approved", "render": function (data, type, row, meta) {
                        if (row['approved'] == 1) {
                            return '<div class="status status--success">{{ __('Approved') }}</div>';
                        }
                        return '<div class="status status--warning">{{ __('Not approved') }}</div>';
                    }
                },

            ],
            @include('partials.lang_datatable')
        });


        $('body').on('submit', '.filter-form', function (e) {
            e.preventDefault();
            table.ajax.reload();

        })
        //*initialize basic datatablee
    </script>
@endpush