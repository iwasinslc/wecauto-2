<?php
$user = $telegramUser->user;

if (null == $user) {
    die('ok');
}
?>
{{__('I sent the code to')}} <b>{{ $user->email }}</b>, {{__('please enter it')}}.